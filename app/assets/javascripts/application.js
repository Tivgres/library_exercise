//= require rails-ujs
//= require turbolinks
//= require jquery3
//= require popper
//= require bootstrap
//= require_tree .

$(function() {
    setTimeout(function() {
        $('.notification').fadeOut('slow');
    }, 3000);
});