function setRating(rating) {
    $('#ratingFormRate').val(rating);
    $('#ratingFormSubmit').click();
}

function triggerHover() {
    $('.rating span').each(function () {
        $(this).hover(
            function () {
                $(this).addClass('golden');
                $(this).prevAll().addClass('golden');
                $(this).nextAll().addClass('darken');

            }, function () {
                $(this).removeClass('golden');
                $(this).nextAll().removeClass('golden darken');
                $(this).prevAll().removeClass('darken golden');
            }
        );
    });
}

$(document).on('turbolinks:load', function() {
    triggerHover();
});