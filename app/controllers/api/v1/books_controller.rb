# frozen_string_literal: true

module Api
  module V1
    class BooksController < ApplicationController

      def index
        @books = Book.page(params[:page]).per(20)
        @top_books = Book.order(votes_count: :desc, histories_count: :desc).limit(5)
      end

      def show
        @book = Book.find(params[:id])
        @comments = @book.comments.order_by(created_at: :desc).page(params[:page]).per(10)
        @rating = @book.current_rate(current_user.id.to_s)
      end

      def image
        @book = Book.find(params[:book_id])
        self.response_body = if params[:version]
                               @book.image.send(params[:version].to_sym).read
                             else
                               @book.image.read
                             end
        self.content_type = @book.image.content_type
      rescue
        head :not_found
      end

      private

      def book_params
        params.require(:book).permit %i[title author description image]
      end
    end
  end
end
