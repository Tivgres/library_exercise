class CommentsController < ApplicationController

  def create
    @comment = current_user.comments.new comment_params
    @comment.book_id = params[:book_id]
    respond_to do |format|
      if @comment.save
        format.js {}
      else
        format.js { render status: :bad_request }
      end
    end
  end

  private

  def comment_params
    params.require(:comment).permit(:body)
  end
end
