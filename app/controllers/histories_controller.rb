class HistoriesController < ApplicationController

  def create
    @book = Book.find(params[:book_id])
    if @book.taken && history_params[:taken] == 'true' # wtf?
      respond_to do |format|
        format.js { render status: :bad_request }
      end
    else
      @history = current_user.histories.new history_params
      @history.book_id = @book.id
      @book = @history.book
      respond_to do |format|
        if @history.save
          format.js {}
        else
          format.js { render status: :bad_request }
        end
      end
    end
  end

  private

  def history_params
    params.require(:history).permit(:taken)
  end
end
