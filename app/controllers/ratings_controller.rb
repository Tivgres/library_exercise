# frozen_string_literal: true

class RatingsController < ApplicationController

  def create
    @book = Book.find(params[:book_id])
    if @book.current_rate(current_user.id).nil?
      @rating = current_user.ratings.new rating_params
      @rating.book_id = @book.id
      respond_to do |format|
        if @rating.save
          format.js {}
        else
          format.js { render status: :bad_request }
        end
      end
    else
      respond_to do |format|
        format.js { render status: :bad_request }
      end
    end
  end

  def update
    @rating = Rating.find(params[:id])
    @book = @rating.book
    respond_to do |format|
      if @rating.update rating_params
        format.js {}
      else
        format.js { render status: :bad_request }
      end
    end
  end

  private

  def rating_params
    params.require(:rating).permit(:rating)
  end
end
