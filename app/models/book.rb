# frozen_string_literal: true

class Book
  include Mongoid::Document
  include Mongoid::Timestamps
  mount_uploader :image, ImageUploader

  field :title, type: String
  field :author, type: String
  field :description, type: String
  field :user_id, type: String
  field :votes, type: Integer, default: 0
  field :taken, type: Boolean, default: false
  field :taker_id, type: String, default: nil
  field :image, type: BSON::ObjectId

  belongs_to :user
  has_many :histories, dependent: :destroy
  has_many :ratings, dependent: :destroy
  has_many :comments, dependent: :destroy

  validates :title, :author, :description, :taken, presence: true

  def rating
    return 0 unless ratings.present?
    (ratings.map(&:rating).inject(0, &:+) / ratings.size).round
  end

  def current_rate(user_id)
    Rating.where(user_id: user_id, book_id: id)&.first
  end
end