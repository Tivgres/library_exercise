# frozen_string_literal: true

class Comment
  include Mongoid::Document
  include Mongoid::Timestamps

  field :body, type: String
  field :user_id, type: String
  field :book_id, type: String

  belongs_to :book, counter_cache: :comments_count
  belongs_to :user

  validates :user_id, :book_id, :body, presence: true
end
