# frozen_string_literal: true

class History
  include Mongoid::Document

  after_save :change_book_state

  field :taken, type: Boolean
  field :user_id, type: String
  field :book_id, type: String

  belongs_to :book, counter_cache: :histories_count
  belongs_to :user

  validates :user_id, :book_id, :taken, presence: true

  def change_book_state
    book.update(taken: taken, taker_id: taken ? user_id : nil)
  end
end
