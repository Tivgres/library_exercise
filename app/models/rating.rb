# frozen_string_literal: true

class Rating
  include Mongoid::Document

  after_create :incr_votes

  field :user_id, type: String
  field :book_id, type: String
  field :rating, type: Integer, default: 0

  belongs_to :book, counter_cache: :votes_count
  belongs_to :user

  validates :user_id, :book_id, presence: true
  validates :rating, inclusion: { in: 1..5 }

  def incr_votes
    book.inc(votes: 1)
  end
end
