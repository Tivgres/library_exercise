class ImageUploader < CarrierWave::Uploader::Base
  include CarrierWave::MiniMagick

  storage :grid_fs

  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  version :large do
    process resize_to_fit: [600, 600]
  end

  version :thumb do
    process resize_to_fit: [150, 150]
  end

  def size_range
    0..50.megabytes
  end

  def extension_whitelist
    %w[jpg jpeg png]
  end
end