json.extract! book, :id, :created_at, :updated_at
json.url api_v1_book_url(book, format: :json)