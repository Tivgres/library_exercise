json.extract! @book, :id, :created_at, :updated_at
json.url api_v1_book_url(@book, format: :json)

json.title @book.title
json.author @book.author
json.description @book.description
json.rating @book.rating
json.votes @book.votes
json.image api_v1_book_image_url(@book)
json.thumb api_v1_book_image_url(@book, version: :thumb)
json.comments @comments