# frozen_string_literal: true
Kaminari.configure do |config|
  config.default_per_page = 20
  config.window = 0
  config.left = 0
  config.right = 0
end
