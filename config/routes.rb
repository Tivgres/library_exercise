Rails.application.routes.draw do
  namespace :api do
    resources :books, except: %i[edit update destroy] do
      get 'image'
    end
  end
  resources :books, except: %i[edit update destroy] do
    get 'image'
    resources :ratings, only: %i[create update]
    resources :comments, only: :create
    resources :histories, only: :create
  end
  devise_for :users
  authenticated :user do
    root 'books#index', as: :authenticated_root
  end
  root 'welcome#index'
end
