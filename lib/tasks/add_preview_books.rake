namespace :add_preview_books do
  task add_data: :environment do
    User.create(email: 'email@example.com', password: rand(50_555).to_s * 20)
    50.times do |i|
      p "Creating book # #{i}"
      User.first.books.new(title: "Some long book title #{i}", author: "Some author of this book #{i}",
                           description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.#{i}",
                           image: File.open(File.join(Rails.root, '/app/assets/images/book.png'))).save!

      30.times do |x|
        p "Creating at book # #{i} comment # #{x}"
        Book.last.comments.new(body: "I like this book # #{x}", user_id: User.first.id)
      end
      10.times do
        next unless rand(2).even?
        p "Creating at book # #{i} taken & revert"
        History.create(taken: true, user_id: User.first.id, book_id: Book.last.id)
        History.create(taken: false, user_id: User.first.id, book_id: Book.last.id)
      end
      p 'Creating rating'
      Rating.create(rating: rand(5), user_id: User.first.id, book_id: Book.last.id)
    end
  end
end
