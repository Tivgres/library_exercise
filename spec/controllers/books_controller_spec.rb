require 'rails_helper'

RSpec.describe BooksController, type: :controller do

  before do
    user = build(:user_full)
    user.save
    book = build(:book_full)
    book.save
    sign_in user
  end

  describe 'GET #index' do
    it 'returns http success at welcome page' do
      get :index, params: {}
      expect(response.status).to eq(200)
      expect(assigns(:books).size).to be(1)
    end
  end

  describe 'GET #new' do
    it 'returns a success response' do
      get :new
      expect(response.status).to eq(200)
      expect(assigns(:book).user).to eq(nil)
    end
  end

  describe 'POST #create' do
    it 'returns a success response' do
      expect do
        post :create, params: { book: attributes_for(:book_full) }
      end.to change(Book, :count).by(1)
      expect(response.status).to eq(302)
      expect(assigns(:book)).to eq(Book.last)
    end

    it 'returns a bad response' do
      expect do
        post :create, params: { book: attributes_for(:book_with_title_descr_image) }
      end.to change(Book, :count).by(0)
      expect(response.status).to eq(400)
    end

    it 'returns a bad response' do
      expect do
        post :create, params: { book: attributes_for(:book_with_title_descr) }
      end.to change(Book, :count).by(0)
      expect(response.status).to eq(400)
    end

    it 'returns a bad response' do
      expect do
        post :create, params: { book: attributes_for(:book_with_title) }
      end.to change(Book, :count).by(0)
      expect(response.status).to eq(400)
    end

    it 'returns a bad response' do
      expect do
        post :create, params: { book: attributes_for(:book_empty) }
      end.to change(Book, :count).by(0)
      expect(response.status).to eq(400)
    end
  end

  describe 'GET #show' do
    it 'returns a success response' do
      get :show, params: { id: Book.last.to_param }
      expect(response.status).to eq(200)
      expect(assigns(:book)).to eq(Book.last)
    end
  end
end
