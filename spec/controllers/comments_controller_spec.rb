require 'rails_helper'

RSpec.describe CommentsController, type: :controller do

  before do
    user = build(:user_full)
    user.save
    book = build(:book_full)
    book.save
    sign_in user
    @request.env['HTTP_ACCEPT'] = 'text/javascript; charset=utf-8'
  end

  describe 'POST #create' do
    it 'returns a success response' do
      expect do
        post :create, params: { comment: attributes_for(:comment_full), book_id: Book.last.id.to_s }
      end.to change(Comment, :count).by(1)
      expect(response.headers['Content-Type']).to eq('text/javascript; charset=utf-8')
      expect(response.status).to eq(200)
    end

    it 'returns a bad response' do
      expect do
      post :create, params: { comment: attributes_for(:comment_without_body), book_id: Book.last.id.to_s }
      end.to change(Comment, :count).by(0)
      expect(response.headers['Content-Type']).to eq('text/javascript; charset=utf-8')
      expect(response.status).to eq(400)
    end
  end
end
