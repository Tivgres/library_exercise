require 'rails_helper'

RSpec.describe HistoriesController, type: :controller do

  before do
    user = build(:user_full)
    user.save
    book = build(:book_full)
    book.save
    book_taken = build(:book_taken)
    book_taken.save
    sign_in user
    @request.env['HTTP_ACCEPT'] = 'text/javascript; charset=utf-8'
  end

  describe 'POST #create' do
    it 'returns a success response FOR TAKERS' do
      expect do
        post :create, params: { history: { taken: true }, book_id: Book.where(taken: false).first.id.to_s }
      end.to change(History, :count).by(1)
      expect(response.headers['Content-Type']).to eq('text/javascript; charset=utf-8')
      expect(response.status).to eq(200)
      expect(Book.first.taken).to be_truthy
      expect(Book.first.taker_id).to eq(User.last.id.to_s)
    end

    it 'returns a success response' do
      expect do
        post :create, params: { history: { taken: false }, book_id: Book.where(taken: true).first.id.to_s }
      end.to change(History, :count).by(1)
      expect(response.headers['Content-Type']).to eq('text/javascript; charset=utf-8')
      expect(response.status).to eq(200)
      expect(Book.first.taken).to be_falsey
    end

    it 'returns a bad response FOR TAKERS' do
      expect do
        post :create, params: { history: { taken: true }, book_id: Book.where(taken: true).first.id.to_s }
      end.to change(History, :count).by(0)
      expect(response.headers['Content-Type']).to eq('text/javascript; charset=utf-8')
      expect(response.status).to eq(400)
    end
  end
end
