require 'rails_helper'

RSpec.describe RatingsController, type: :controller do

  before do
    user = build(:user_full)
    user.save
    book = build(:book_full)
    book.save
    sign_in user
    @request.env['HTTP_ACCEPT'] = 'text/javascript; charset=utf-8'
  end

  describe 'POST #create' do
    it 'returns a success response' do
      expect do
        post :create, params: { rating: attributes_for(:rating_nice), book_id: Book.last.id.to_s }
      end.to change(Rating, :count).by(1)
      expect(response.headers['Content-Type']).to eq('text/javascript; charset=utf-8')
      expect(response.status).to eq(200)
      expect(Book.last.rating).to eq(5)
    end

    it 'returns a bad response' do
      expect do
        post :create, params: { rating: attributes_for(:rating_less_zero), book_id: Book.last.id.to_s }
      end.to change(Rating, :count).by(0)
      expect(response.headers['Content-Type']).to eq('text/javascript; charset=utf-8')
      expect(response.status).to eq(400)
    end

    it 'returns a bad response' do
      expect do
        post :create, params: { rating: attributes_for(:rating_more_five), book_id: Book.last.id.to_s }
      end.to change(Rating, :count).by(0)
      expect(response.headers['Content-Type']).to eq('text/javascript; charset=utf-8')
      expect(response.status).to eq(400)
    end
  end

  describe 'PATCH #update' do
    it 'returns a success response' do
      build(:user_full).save
      build(:book_full).save
      build(:rating_exist).save
      expect do
        patch :update, params: {
            rating: attributes_for(:rating_nice),
            book_id: Book.last.id.to_s,
            id: Rating.last.id.to_s
        }
      end.to change(Comment, :count).by(0)
      expect(response.headers['Content-Type']).to eq('text/javascript; charset=utf-8')
      expect(response.status).to eq(200)
      expect(Rating.last.rating).to eq(5)
    end

    it 'returns a bad response' do
      build(:user_full).save
      build(:book_full).save
      build(:rating_exist).save
      expect do
        patch :update, params: {
            rating: attributes_for(:rating_less_zero),
            book_id: Book.last.id.to_s,
            id: Rating.last.id.to_s
        }
      end.to change(Comment, :count).by(0)
      expect(response.headers['Content-Type']).to eq('text/javascript; charset=utf-8')
      expect(response.status).to eq(400)
      expect(Rating.last.rating).to eq(1)
    end

    it 'returns a bad response' do
      build(:user_full).save
      build(:book_full).save
      build(:rating_exist).save
      expect do
        patch :update, params: {
            rating: attributes_for(:rating_more_five),
            book_id: Book.last.id.to_s,
            id: Rating.last.id.to_s
        }
      end.to change(Comment, :count).by(0)
      expect(response.headers['Content-Type']).to eq('text/javascript; charset=utf-8')
      expect(response.status).to eq(400)
      expect(Rating.last.rating).to eq(1)
    end
  end
end
