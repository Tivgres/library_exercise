require 'rails_helper'

RSpec.describe WelcomeController, type: :controller do

  describe 'GET #index' do
    it 'returns http success at welcome page' do
      get :index
      expect(response.status).to eq(200)
    end
  end
end
