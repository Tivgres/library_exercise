require 'faker'

FactoryBot.define do

  factory :book do

    factory :book_empty, parent: :book do
      user_id { build(:user_full).id }
    end

    factory :book_with_title, parent: :book do
      user_id { build(:user_full).id }
      title { Faker::Book.title }
    end

    factory :book_with_title_descr, parent: :book do
      user_id { build(:user_full).id }
      title { Faker::Book.title }
      description { Faker::Lorem.paragraph }
    end

    factory :book_with_title_descr_image, parent: :book do
      user_id { build(:user_full).id }
      title { Faker::Book.title }
      description { Faker::Lorem.paragraph }
      image do
        Rack::Test::UploadedFile.new(Rails.root.join('app/assets/images', 'book.png'), 'image/png')
      end
    end

    factory :book_full, parent: :book do
      user_id { User.last.id }
      title { Faker::Book.title }
      description { Faker::Lorem.paragraph }
      image do
        Rack::Test::UploadedFile.new(Rails.root.join('app/assets/images', 'book.png'), 'image/png')
      end
      author { Faker::Book.author }
    end

    factory :book_taken, parent: :book do
      taken { true }
      taker_id { User.last.id }
      user_id { User.last.id }
      title { Faker::Book.title }
      description { Faker::Lorem.paragraph }
      image do
        Rack::Test::UploadedFile.new(Rails.root.join('app/assets/images', 'book.png'), 'image/png')
      end
      author { Faker::Book.author }
    end
  end
end