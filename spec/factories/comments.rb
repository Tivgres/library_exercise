# frozen_string_literal: true

require 'faker'

FactoryBot.define do
  factory :comment do

    factory :comment_without_body, parent: :comment do
      body { '' }
    end

    factory :comment_full, parent: :comment do
      body { Faker::Lorem.paragraph }
    end
  end
end