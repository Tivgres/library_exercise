# frozen_string_literal: true

require 'faker'

FactoryBot.define do
  factory :rating do

    factory :rating_less_zero, parent: :rating do
      rating { -555 }
    end

    factory :rating_more_five, parent: :rating do
      rating { 555 }
    end

    factory :rating_nice, parent: :rating do
      rating { 5 }
    end

    factory :rating_exist, parent: :rating do
      user_id { User.last.id.to_s }
      book_id { Book.last.id.to_s }
      rating { 1 }
    end
  end
end