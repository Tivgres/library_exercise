# frozen_string_literal: true

require 'faker'

FactoryBot.define do
  factory :user do

    factory :user_without_email, parent: :user do
      password { Faker::Internet.password }
    end

    factory :user_without_password, parent: :user do
      email { Faker::Internet.email }
    end

    factory :user_full, parent: :user do
      email { Faker::Internet.email }
      password { Faker::Internet.password }
    end
  end
end